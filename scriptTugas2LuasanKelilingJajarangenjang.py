print('\nTugas 2')
print('Mengukur Luasan dan Keliling Jajarangenjang\n')

# Deklarasi variabel sebagai input user, konversi ke tipe data float (membutuhkan ketepatan desimal) 
sisiAtas = float(input("Masukkan besar sisi atas (dalam meter): "))
sisiBawah = float(input("Masukkan besar sisi bawah (dalam meter): "))
tinggi = float(input("Masukkan besar tinggi (dalam meter): "))

# Hitungan aritmatika dalam pengukuran 
luasan = float(sisiBawah * tinggi)
keliling = 2 * float(sisiAtas + sisiBawah)

# Tampilan hasil dari luasan dan keliling beserta informasi input user dengan ketepatan 4 desimal
print("\nBesar Sisi Atas: " + format(sisiAtas, '.4f') + ' meter')
print("Besar Sisi Bawah: " + format(sisiBawah, '.4f') + ' meter')
print("\nBesar Tinggi: " + format(tinggi, '.4f') + ' meter')

print("\nHasil besar Luasan: " + format(luasan, '.4f') + ' meter persegi')
print("\nHasil besar Keliling: " + format(keliling, '.4f') + ' meter')

print('\nTerima Kasih. Satrio A. Cesiojakty\n')

