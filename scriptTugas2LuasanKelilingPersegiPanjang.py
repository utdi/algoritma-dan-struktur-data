print('\nTugas 2')
print('Mengukur Luasan dan Keliling Persegi Panjang\n')

# Deklarasi variabel sebagai input user, konversi ke tipe data float (membutuhkan ketepatan desimal) 
panjang = float(input("Masukkan besar panjang (dalam meter): "))
lebar = float(input("Masukkan besar lebar (dalam meter): "))

# Hitungan aritmatika dalam pengukuran 
luasan = float(panjang * lebar)
keliling = 2 * float(panjang + lebar)

# Tampilan hasil dari luasan dan keliling beserta informasi input user dengan ketepatan 4 desimal
print("\nPanjang: " + format(panjang, '.4f') + ' meter')
print("Lebar: " + format(lebar, '.4f') + ' meter')
print("\nHasil besar Luasan: " + format(luasan, '.4f') + ' meter persegi')
print("\nHasil besar Keliling: " + format(keliling, '.4f') + ' meter')

print('\nTerima Kasih. Satrio A. Cesiojakty\n')

