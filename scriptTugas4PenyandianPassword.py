print('\nTugas 4')
print('Penyandian Password\n')

# Deklarasi variabel sebagai input user, konversi ke tipe data string
password = str(input("Masukkan password : "))

# Deklarasi Penyandian
penyandian = {
    'A': 'x',
    'B': '*',
    'C': '#',
    'D': 'v',
    'E': 'a',
    'F': 'm',
    'G': 'q',
    'H': '%'
}

# Perulangan dan penggabungan karakter
tersandi = ''.join(
    '?' if idx not in penyandian else penyandian[idx] for idx in password)

# Informasi Hasil
print("Tersandikan : " + str(tersandi))
