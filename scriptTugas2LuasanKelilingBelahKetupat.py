print('\nTugas 2')
print('Mengukur Luasan dan Keliling Belah Ketupat\n')

# Deklarasi variabel sebagai input user, konversi ke tipe data float (membutuhkan ketepatan desimal) 
sisi = float(input("Masukkan besar sisi (dalam meter): "))
diagonal1 = float(input("Masukkan besar diagonal 1 (dalam meter): "))
diagonal2 = float(input("Masukkan besar diagonal 2 (dalam meter): "))

# Hitungan aritmatika dalam pengukuran 
luasan = float(diagonal1 * diagonal2) / 2
keliling = 4 * float(sisi)

# Tampilan hasil dari luasan dan keliling beserta informasi input user dengan ketepatan 4 desimal
print("\nBesar Sisi Atas: " + format(sisi, '.4f') + ' meter')
print("Besar Diagonal 1: " + format(diagonal1, '.4f') + ' meter')
print("\nBesar Diagonal 2: " + format(diagonal2, '.4f') + ' meter')

print("\nHasil besar Luasan: " + format(luasan, '.4f') + ' meter persegi')
print("\nHasil besar Keliling: " + format(keliling, '.4f') + ' meter')

print('\nTerima Kasih. Satrio A. Cesiojakty\n')

