print('\nTugas 3')
print('Pencarian Nilai Terkecil dan Terbesar\n')

# Deklarasi variabel sebagai input user, konversi ke tipe data int
a = int(input("Masukkan besar nilai a : "))
b = int(input("Masukkan besar nilai b : "))
c = int(input("Masukkan besar nilai c : "))
d = int(input("Masukkan besar nilai d : "))

# Deklarasi list untuk memudahkan pemanggilan berdasar atribut
kumpulanNilai = {
    'Nilai A': a,
    'Nilai B': b,
    'Nilai C': c,
    'Nilai D': d,
}

print()

# Kombinasi for dan if elif, dalam pencarian nilai maksimum/minimum
for atribut, nilai in kumpulanNilai.items():
    if nilai == max(kumpulanNilai.values()):
        print(f"Nilai terbesar adalah {atribut} ( {nilai} )")
    elif nilai == min(kumpulanNilai.values()):
        print(f"Nilai terkecil adalah {atribut} ( {nilai} )")

print('\nTerima Kasih. Satrio A. Cesiojakty\n')
