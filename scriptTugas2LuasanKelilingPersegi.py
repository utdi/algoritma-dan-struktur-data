print('\nTugas 2')
print('Mengukur Luasan dan Keliling Persegi\n')

# Deklarasi variabel sebagai input user, konversi ke tipe data float (membutuhkan ketepatan desimal) 
sisi = float(input("Masukkan besar sisi (dalam meter): "))

# Hitungan aritmatika dalam pengukuran 
luasan = float(sisi * sisi)
keliling = 4 * float(sisi)

# Tampilan hasil dari luasan dan keliling beserta informasi input user dengan ketepatan 4 desimal
print("\nBesar Sisi: " + format(sisi, '.4f') + ' meter')
print("\nHasil besar Luasan: " + format(luasan, '.4f') + ' meter persegi')
print("\nHasil besar Keliling: " + format(keliling, '.4f') + ' meter')

print('\nTerima Kasih. Satrio A. Cesiojakty\n')

