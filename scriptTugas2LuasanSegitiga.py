print('\nTugas 2')
print('Mengukur Luasan Segitiga\n')

# Deklarasi variabel sebagai input user, konversi ke tipe data float (membutuhkan ketepatan desimal) 
alas = float(input("Masukkan besar alas (dalam meter): "))
tinggi = float(input("Masukkan besar tinggi (dalam meter): "))

# Hitungan aritmatika dalam pengukuran luasan segitiga 
luasan = float(alas * tinggi) / 2

# Tampilan hasil dari luasan beserta informasi input user dengan ketepatan 4 desimal
print("\nAlas: " + format(alas, '.4f') + ' meter')
print("Tinggi: " + format(tinggi, '.4f') + ' meter')
print("\nHasil besar Luasan: " + format(luasan, '.4f') + ' meter persegi')

print('\nTerima Kasih. Satrio A. Cesiojakty\n')

