print('\nTugas 2')
print('Mengukur Luasan dan Keliling Trapesium\n')

# Deklarasi variabel sebagai input user, konversi ke tipe data float (membutuhkan ketepatan desimal) 
sisi1 = float(input("Masukkan besar sisi 1 (dalam meter): "))
sisi2 = float(input("Masukkan besar sisi 2 (dalam meter): "))
sisi3 = float(input("Masukkan besar sisi 3 (dalam meter): "))
sisi4 = float(input("Masukkan besar sisi 4 (dalam meter): "))
tinggi = float(input("Masukkan besar tinggi (dalam meter): "))

# Hitungan aritmatika dalam pengukuran 
luasan = float(sisi1 + sisi2) * float(tinggi) / 2
keliling = float(sisi1 + sisi2 + sisi3 + sisi4)

# Tampilan hasil dari luasan dan keliling beserta informasi input user dengan ketepatan 4 desimal
print("\nBesar Sisi 1: " + format(sisi1, '.4f') + ' meter')
print("Besar Sisi 2: " + format(sisi2, '.4f') + ' meter')
print("Besar Sisi 3: " + format(sisi3, '.4f') + ' meter')
print("Besar Sisi 4: " + format(sisi4, '.4f') + ' meter')
print("Besar Tinggi: " + format(tinggi, '.4f') + ' meter')

print("\nHasil besar Luasan: " + format(luasan, '.4f') + ' meter persegi')
print("\nHasil besar Keliling: " + format(keliling, '.4f') + ' meter')

print('\nTerima Kasih. Satrio A. Cesiojakty\n')

